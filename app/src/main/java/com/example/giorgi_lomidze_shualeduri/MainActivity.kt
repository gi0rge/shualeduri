package com.example.giorgi_lomidze_shualeduri

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.CheckBox
import android.widget.EditText
import android.widget.Toast

class MainActivity : AppCompatActivity() {
    private lateinit var userName: EditText
    private lateinit var userSurName: EditText
    private lateinit var mobileNum: EditText
    private lateinit var personalNum: EditText

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        userName = findViewById(R.id.userName)
        userSurName = findViewById(R.id.userSurName)
        mobileNum = findViewById(R.id.mobileNum)
        personalNum = findViewById(R.id.personalNum)

    }
    fun checkReg(view: View) {
        if(userName.length() < 3 || userName.toString().isBlank())  {
            val message = "მომხმარებლის სახელი უნდა შედგებოდეს მინიმუმ 3 სიმბოლოსგან"
            Toast.makeText(applicationContext, message, Toast.LENGTH_SHORT).show()
            return;
        }
        else if(userSurName.length() < 5 || userSurName.toString().isBlank())  {
            val message = "მომხმარებლის გვარი უნდა შედგებოდეს მინიმუმ 5 სიმბოლოსგან"
            Toast.makeText(applicationContext, message, Toast.LENGTH_SHORT).show()
            return;
        }
        else if(mobileNum.length() != 9)  {
            val message = "ტელეფონის ნომერი უნდა შედგებოდეს 9 ციფრისაგან და იწყებოდეს 5-ით"
            Toast.makeText(applicationContext, message, Toast.LENGTH_SHORT).show()
            return;
        }
        else if(personalNum.length() != 11)  {
            val message = "პირადი ნომერი უნდა შედგებოდეს 11 ციფრისაგან"
            Toast.makeText(applicationContext, message, Toast.LENGTH_SHORT).show()
            return;
        }
        val message = "რეგისტრაცია წარმატებით დასრულდა"
        Toast.makeText(applicationContext, message, Toast.LENGTH_SHORT).show()
    }
}